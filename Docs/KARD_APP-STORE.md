# Application state management

## @kard/app-store

`@kard/app-store` is a reactive store that is using to keep the application state. Its basic usage pattern is:
```js
import Store from '@kard/app-store';
const initialState = { keyA: 'valA', keyB: 'valB' }
const store = new Store(initialState);
store.setState({ key_name: 'key value' })
store.getState() 
// { keyA: 'valA', keyB: 'valB', key_name: 'key value' }
```

A `store` has the following properties:

### subscribe

`subscribe` allows getting subscribed for the store changes. As the only argument, it takes a `subscriber` function which is called each time when the store got updated. With it, the `subscriber` got the only argument of the `Store` shape. The `subscribe` returns `unsubscribe` function which should be used to stop the subscription when necessary.

```js
const unsubscribe = state
  .subscribe(($store) => console
    .info('updated state', $store.getState()));
store.setState({ key_name: 'key value' })
unsubscribe(); // release the subscription
```

### use

`use` takes a middleware function which will be called for each state update with basic (previous) state value and update values in parameters.

A middleware function has to return update which will be passed to the next middleware and, in the end, applied to the basic state. If a middleware modifies the update, the updated version will be used for the followed middleware and state updates.

All middlewares are invocated in the assignment order.

`use` returns unsubscribe method which can be used to remove middleware.


### setState

`setState` takes the partial state object and update the application state with it.


### getState

`getState` takes no arguments or the state mapping object. It returns the whole state when no arguments are given. When the mapper is passed it takes the value map.

A mapper is an object which key of each is a variable and its value is the dot-separated path to the appropriate value in the state.

```js
// initialState = { varA: 1, varsetA: { varB: 2, varC: 3 } }
const stateMap = { 
    a: 'varA', 
    b: 'varsetA.varB', 
    c: 'varsetA.varC', 
    d: 'varsetA' 
}
const store = new Store(initialState)
const result = store.getState(stateMap)
// result === { a: 1, b: 2, c:3, d:{ varB: 2, varC: 3 } }
```

### Advanced example

```js
import State from '@kard/app-store';

export default function Model(config) {
  /* eslint-disable no-console */
  const logMiddleware = (getState) => (update) => {
    console.group('setState');
    console.log('update', update);
    console.log('state', getState());
    console.groupEnd();
    return update;
  };

  const state = new State(initialState);
  state.use(logMiddleware);

  state.subscribe(($store) => config.logger.info(
    'NEW STATE', 
    $store.getState(),
  ));
  return state;
}

```

