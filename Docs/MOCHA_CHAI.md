```sh
# install tools and dependencies
npm add mocha chai @babel/register -D

# run mocha/chai with babel support
npx mocha --require @babel/register './{,!(node_modules)/**}/*.spec.js'
```
