## Free APIs

* [rapidapi.com](https://rapidapi.com/)
Find and Connect to Thousands of APIs

* [PokeAPI](https://pokeapi.co/) 
У крупнейшей медиа-франшизы всех времен есть простой способ получить данные о 800+ покемонах.

* [NASA API](https://api.nasa.gov) Получите данные об астероидах, галактиках и многом другом.

* [Open Food Facts](https://world.openfoodfacts.org/data) 
Огромное количество данных о продуктах питания со всего мира.

* [TransLoc OpenAPI](https://rapidapi.com/transloc/api/openapi-1-2) 
Получите живые данные об общественном транспорте городов и кампусов.

* [Urban Dictionary API](https://rapidapi.com/community/api/urban-dictionary)

* [Merriam-Webster Dictionary API](https://dictionaryapi.com/) 
Для тех, кому нужны определения и синонимы реальных слов.

* [Numbers API](http://numbersapi.com/#42) 
Интересные факты и вопросы о числах.

* [WeatherBit API](https://www.weatherbit.io/api) 
Текущие и исторические данные о погоде.

* [US Government Data API](https://www.data.gov/developers/apis) 
Достаточно большой набор данных о Соединенных Штатах – сельское хозяйство, здравоохранение, общественная безопасность и т.д.

* [Bible API](https://scripture.api.bible/) 
Самая продаваемая книга всех времен. Величайшая история.
