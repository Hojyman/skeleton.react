## API mocks

* [jsonplaceholder](https://jsonplaceholder.typicode.com)
Fake Online REST API for Testing and Prototyping.


* [httpstat.us](http://httpstat.us)
A super simple service for generating different HTTP codes.
