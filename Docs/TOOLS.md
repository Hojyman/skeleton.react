## Useful tools

* [gitcdn.xyz](https://gitcdn.xyz/) - GitCDN serves raw files directly from GitHub with proper Content-Type headers and a super fast. Always get the latest commit.

* [svgomg on github](https://jakearchibald.github.io/svgomg/)
* [svgomg on firebaseapp](https://svgomg.firebaseapp.com/)
SVG Optimizer is a Nodejs-based tool for optimizing SVG vector graphics files.

* [icomoon.io](https://icomoon.io/)
IcoMoon is an icon solution, providing three main services: Vector Icon Packs, The IcoMoon App, and hosting icons as SVGs or fonts. 
