import React from 'react';
import ReactDOM from 'react-dom';

import AppContext, { connect } from '@kard/react-app-context';

import ErrorBoundary from './ErrorBoundary';

/* eslint-disable react/prop-types, no-console, react/button-has-type */

function View(props) {
  // console.log('VIEW PROPS', props);
  const {
    config, count, onSetCount, onGetApiError,
  } = props;
  return (
    <div>
      <h1>{ `${config.pkg.name} ${config.pkg.version} (${config.pkg['version-stamp']})` }</h1>
      <h2>{ count }</h2>
      <button onClick={() => onSetCount(count + 1)}>++</button>
      <button onClick={() => onSetCount(count - 1)}>--</button>
      <br />
      <button onClick={() => onGetApiError(500 + count)}>Get API error</button>
    </div>
  );
}

const mapStateToProps = (appContext, ownProps) => {
  // console.log('MAP PROPS', { appContext, ownProps });

  // get state map state
  const stateMap = appContext.mapState({
    count: 'example.count',
    slideshow: 'slideshow',
  });

  // add actions/events handlers
  const onSetCount = (count) => appContext.controller.example.handleSet({ count });
  const onGetApiError = (errCode) => appContext.controller.example.handleGetApiError(errCode);

  return {
    config: appContext.config,

    onSetCount,
    onGetApiError,

    ...stateMap,
    ...ownProps,
  };
};

const ConnectedView = connect(mapStateToProps)(View);

const App = (props) => {
  const { config, $model, controller } = props;
  // console.log('APP PROPS', props);
  return (
    <ErrorBoundary>
      <AppContext value={{ config, $model, controller }}>
        <ConnectedView />
      </AppContext>
    </ErrorBoundary>
  );
};

export default function (config, $model, controller) {
  ReactDOM.render(
    <App config={config} $model={$model} controller={controller} />,
    document.getElementById('root'),
  );
}
