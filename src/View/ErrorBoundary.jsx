import React from 'react';
import PropTypes from 'prop-types';
import { uuidv4 } from '@kard/utils';

/* eslint-disable react/prop-types */
function ErrorBoundarySkin({
  children,
  errorId,
}) {
  if (errorId) {
    return (
      <div className="ErrorBoundary">
        <div>
          <h1>Oops something went wrong :(</h1>
          <h2>{ `Ref: ${errorId}` }</h2>
        </div>
      </div>
    );
  }
  return children;
}

export function mapErrorToLog(errorId, error, info) {
  const msg = {
    type: 'ErrorBoundary',
    message: error.message,
    stack: error.stack,
    href: window.location.href,
    ...info,
  };
  return { errorId, level: 'critical', info: msg };
}

export default class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { errorId: null };
  }

  // eslint-disable-next-line no-unused-vars
  static getDerivedStateFromError(error) {
    return { errorId: uuidv4() };
  }

  componentDidCatch(error, errorInfo) {
    const { errorId } = this.state;
    /*
      NOTE: here is the perfect place to send
      error notification to a log endpoint
    */
    // eslint-disable-next-line no-console
    console.log(mapErrorToLog(errorId, error, errorInfo));
  }

  render() {
    const { errorId } = this.state;
    const { children } = this.props;

    return (
      <ErrorBoundarySkin
        errorId={errorId}
      >
        { children }
      </ErrorBoundarySkin>
    );
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.node,
};

ErrorBoundary.defaultProps = {
  children: null,
};
