export default function Config() {
  /* eslint-disable */
  this.logger = console;
  this.appSettings = require(`../config.${process.env.NODE_ENV}.js`).default
  this.pkg = require('package.json');
}
