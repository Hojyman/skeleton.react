import Config from './Config';
import Controller from './Controller';
import Model from './Model';
import View from './View';

const config = new Config();
const $model = new Model(config);
const controller = new Controller(config, $model);

export default new View(config, $model, controller);
