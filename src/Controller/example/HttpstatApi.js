// fetch("http://httpstat.us/500")
import { handleHttpErrors } from './api.utils';

export default function HttpstatApi(baseUrl) {
  return {
    getErr: (errCode) => fetch(`${baseUrl}/${errCode}`)
      .then(handleHttpErrors)
      .then((response) => response.json()),
  };
}
