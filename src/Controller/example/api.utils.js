/* eslint-disable import/prefer-default-export */

// https://www.tjvantoll.com/2015/09/13/fetch-and-errors/
// http://httpstat.us/

export function handleHttpErrors(response) {
  if (!response.ok) {
    // throw Error(response.statusText);
    // throw Error(response.status);
    throw response;
  }
  return response;
}
