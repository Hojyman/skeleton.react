import { handleHttpErrors } from './api.utils';

export default function PlaceholderApi(baseUrl) {
  return {
    getToDo: (id) => fetch(`${baseUrl}/todos/${id}`)
      .then(handleHttpErrors)
      .then((response) => response.json()),
  };
}
