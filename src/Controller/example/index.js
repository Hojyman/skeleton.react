/* eslint-disable import/prefer-default-export */

import HttpstatApi from './HttpstatApi';
import PlaceholderApi from './PlaceholderApi';

export function Api(config) {
  return {
    placeholder: new PlaceholderApi(config.api.placeholder),
    httpstat: new HttpstatApi(config.api.httpstat),
  };
}
