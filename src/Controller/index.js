import { Api } from './example';

// eslint-disable-next-line no-console
const log = (...x) => console.log('[api]', ...x);


export default function Controller(config, $model) {
  const api = new Api(config.appSettings);

  return {
    example: {
      handleSet: (vals) => {
        $model.setState({ example: vals });
        const id = vals.count;

        api.placeholder.getToDo(id)
          .then((json) => log('JSON', json))
          .catch((err) => log('ERR', err));
      },
      handleGetApiError: (errCode) => {
        api.httpstat.getErr(errCode)
          .then((json) => log('JSON', json))
          .catch((err) => log('ERR', err));
      },
    },
  };
}
