// eslint-disable-next-line import/no-unresolved
import State from '@kard/app-store';

const initialState = {
  example: {
    count: 0,
  },
  slideshow: [
    { src: 'https://www.w3schools.com/howto/img_nature_wide.jpg', caption: 'Caption Text' },
    { src: 'https://www.w3schools.com/howto/img_snow_wide.jpg', caption: 'Caption Two' },
    { src: 'https://www.w3schools.com/howto/img_mountains_wide.jpg', caption: 'Caption Three' },
  ],
};

export default function Model(config) {
  // const logMiddlewareA = (getState) => (update) => {
  //   config.logger.info('UPDATE A', getState(), update.example.count);
  //   return {
  //     ...update,
  //     example: { count: update.example.count + 1 },
  //   };
  // };

  // const logMiddlewareB = (getState) => (update) => {
  //   config.logger.info('UPDATE B', getState(), update.example.count);
  //   return {
  //     ...update,
  //     example: { count: update.example.count + 1 },
  //   };
  // };

  const logMiddleware = (getState) => (update) => {
    /* eslint-disable no-console */
    console.group('setState');
    console.log('update', update);
    console.log('state', getState());
    console.groupEnd();
    /* eslint-enable no-console */
    return update;
  };

  const state = new State(initialState);
  state.use(logMiddleware);
  // state.use(logMiddlewareB);
  state.subscribe(($store) => config.logger.info('NEW STATE', $store.getState()));
  return state;
}
