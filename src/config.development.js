// eslint-disable-next-line import/no-unresolved
import { logLevels } from '@kard/bw-logger';

const appSettings = {
  api: {
    placeholder: 'https://jsonplaceholder.typicode.com',
    httpstat: 'http://httpstat.us',
  },
  logLevel: logLevels.DEBUG,
};

export default appSettings;
