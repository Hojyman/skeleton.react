process.env.BABEL_ENV = 'production';
process.env.NODE_ENV = 'production';

const baseConfig = require('./base.config');

module.exports = baseConfig({
  /* put production params here */
});
