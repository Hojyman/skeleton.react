/* eslint-disable no-console */
const fs = require('fs');

const write = (path, data) => new Promise((res, rej) => {
  fs.writeFile(path, JSON.stringify(data, null, ' '), (err) => {
    if (err) { return rej(err); }
    return res();
  });
});


const read = (path) => new Promise((res, rej) => {
  fs.readFile(path, 'utf8', (err, data) => {
    if (err) { return rej(err); }
    return res(JSON.parse(data));
  });
});


module.exports = {
  write,
  read,
};
