const path = require('path');
const json = require('./json');

/* eslint-disable no-underscore-dangle */

const _path = path.join(path.resolve('./'), 'package.json');

const write = (data) => json.write(_path, data);
const read = () => json.read(_path);

module.exports = {
  path: _path,
  write,
  read,
};
