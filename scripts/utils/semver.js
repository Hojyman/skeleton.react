/* eslint-disable import/no-extraneous-dependencies, no-underscore-dangle */
const {
  compareSemVer, isValidSemVer, parseSemVer,
} = require('semver-parser');

const arrToStr = (arr) => {
  if (!arr) { return arr; }
  if (Array.isArray(arr)) { return arr.join('.'); }
  return [arr];
};

function joinSemver({
  major, minor, patch, pre: _pre, build: _build,
}) {
/* eslint-disable no-underscore-dangle */
  const pre = arrToStr(_pre);
  const build = arrToStr(_build);

  const core_ = `${major}.${minor}.${patch}`;
  const pre_ = `${pre ? `-${pre}` : ''}`;
  const build_ = `${build ? `+${build}` : ''}`;
  return `${core_}${pre_}${build_}`;
}

module.exports = {
  joinSemver,
  compareSemVer,
  isValidSemVer,
  parseSemVer,
};
