/* eslint-disable no-restricted-syntax, guard-for-in,  */
module.exports = function cloneObject(aObject) {
  if (
    !aObject
    || (typeof aObject !== 'object')
  ) {
    return aObject;
  }

  let v;
  const bObject = Array.isArray(aObject) ? [] : {};
  for (const k in aObject) {
    v = aObject[k];
    bObject[k] = (typeof v === 'object') ? cloneObject(v) : v;
  }

  return bObject;
};
