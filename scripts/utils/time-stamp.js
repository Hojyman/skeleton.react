/* eslint-disable no-underscore-dangle, max-len, import/prefer-default-export */
const d2 = (num) => (num < 10 ? `0${num}` : `${num}`);
const d3 = (num) => (num < 100 ? `0${d2(num)}` : `${num}`);

// export const getTimeStamp = (d = new Date(), fmt = 'HH:MM:ss.SSS') => fmt
module.exports = (fmt = 'DD.MM.YYYY hh:mm:ss.SSS', d = new Date()) => fmt
  .replace('DD', d2(d.getDate()))
  .replace('MM', d2(d.getMonth()))
  .replace('YYYY', d.getFullYear())
  .replace('YY', d.getFullYear().toString().substr(-2))
  .replace('hh', d2(d.getHours()))
  .replace('mm', d2(d.getMinutes()))
  .replace('ss', d2(d.getSeconds()))
  .replace('SSS', d3(d.getMilliseconds()));
