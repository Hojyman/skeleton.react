const delimiters = ['=', ':'];

const getProcessArgs = () => {
  const result = {};
  process.argv.slice(2)
    .map((arg) => {
      const argl = arg.toLowerCase();
      const del = delimiters.find((d) => arg.indexOf(d) > -1);
      if (!del) { return { key: argl, value: true }; }
      const [key, value] = arg.split(del);
      return { key: key.toLowerCase(), value };
    }).forEach(({ key, value }) => { result[key] = value; });
  return result;
};

module.exports = {
  getProcessArgs,
};
