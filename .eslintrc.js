/* eslint-disable import/no-extraneous-dependencies */
const getBabelOptions = (babelConfig) => (typeof babelConfig === 'function'
  ? babelConfig() : babelConfig);

const getBabelPluginOptions = (babelCfg, pluginName) => {
  const resolver = babelCfg
    .plugins.find((el) => el[0].indexOf(pluginName) > -1);
  return resolver ? resolver[1] : {};
};

const babelOptions = getBabelOptions(require('./.babelrc.js'));

module.exports = {
  extends: [
    'eslint-config-airbnb',
  ],
  parser: require.resolve('babel-eslint'),
  parserOptions: babelOptions,
  plugins: [
    'module-resolver', // eslint-plugin-module-resolver
  ],
  settings: {
    'import/resolver': {
      'babel-module': getBabelPluginOptions(
        babelOptions,
        'babel-plugin-module-resolver',
      ),
    },
  },
  globals: {
    /* Root global for browser extensions */
    chrome: 'readonly',
  },
  rules: {
    'no-unused-vars': ['error', { varsIgnorePattern: 'React' }],
    'import/no-unresolved': 1,
    'import/prefer-default-export': 1,
    'no-underscore-dangle': 1,
    'react/jsx-props-no-spreading': 1,
  },
  env: {
    browser: true,
    node: true,
    jest: true,
  },
  overrides: [
    {
      files: '*.test.js',
      rules: {
        'no-unused-expressions': 'off',
      },
    },
  ],
};
