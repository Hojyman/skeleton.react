## React skeleton

## Features

* Advanced babel/webpack/eslint setup
* Collection of the custom packages
* Minimal qty of external dependencies

NOTE: this repo is simplified version of [MVP project research](https://gitlab.com/Hojyman/edu.new-project-mvp) which can give some additional ideas.

## How to use

```sh
git clone https://gitlab.com/Hojyman/skeleton.react.git project.ui
cd $_
npm update
npm start
```


### Avaliable scripts

`npm start` - webpack-dev-server

`npm run build` - build production artifact

`npm run esfix` - check and fix the code with eslint


## Useful resources

* [Useful tools](Docs/TOOLS.md) 
* [API mocks](Docs/API_MOCKS.md)
* [Free APIs](Docs/API_FREE.md)
