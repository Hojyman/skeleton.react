/* eslint-disable import/no-extraneous-dependencies */
const Mocha = require('mocha');

const runner = new Mocha({});

// eslint-disable-next-line no-console
const { error, log } = console;

runner.addFile('./test.js');

runner.run((failures) => {
  if (failures) {
    error(failures);
  } else {
    log('All passed.');
  }
});
