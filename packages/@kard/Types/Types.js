/* eslint-disable max-classes-per-file */
function BaseType({ test, required }) {
  return {
    get required() {
      return !!required;
    },
    test: (v, ...r) => {
      if (typeof v === 'undefined' && required) { return false; }
      if (typeof v === 'undefined' && !required) { return true; }
      return test(v, ...r);
    },
  };
}

class Type extends BaseType {
  constructor({ test }) {
    super({ test });
    this.isRequired = new BaseType({ test, required: true });
  }
}

class LooseType extends Type {
  constructor({ looseTest, exactTest }) {
    super({ test: exactTest });
    this.looseTest = (x) => typeof x === 'undefined' || looseTest(x);
    this.isRequired = new BaseType({ test: exactTest, required: true });
    this.isRequired.looseTest = looseTest;

    this.loose = new BaseType({ test: looseTest, required: false });
    this.loose.exactTest = (x) => typeof x === 'undefined' || exactTest(x);
    this.loose.isRequired = new BaseType({ test: looseTest, required: true });
    this.loose.isRequired.exactTest = exactTest;
  }
}

const Types = { // default export
  get array() {
    return new Type({
      test: (v) => Array.isArray(v),
    });
  },
  get bool() {
    return new Type({
      test: (v) => typeof v === 'boolean',
    });
  },
  /* func */
  get number() {
    return new Type({
      test: (v) => typeof v === 'number',
    });
  },
  get object() {
    return new Type({
      test: (v) => !Array.isArray(v)
        && typeof v === 'object',
    });
  },
  get string() {
    return new Type({
      test: (v) => typeof v === 'string',
    });
  },
  /* symbol */
  /* node */
  /* element */
  /* elementType */
  instanceOf: (Class) => new Type({
    test: (v) => v instanceof Class,
  }),
  oneOf: (l) => new Type({
    test: (v) => l.indexOf(v) > -1,
  }),
  oneOfType: (l) => new Type({
    test: (v) => l.some((e) => e.test(v)),
  }),
  arrayOf: (s) => new Type({
    test: (v) => {
      if (typeof v !== 'object' || !Array.isArray(v)) {
        return false;
      }

      if (s.required && v.length === 0) {
        return false;
      }

      return v.every((e) => s.test(e));
    },
  }),
  /* objectOf */
  shape: (s) => {
    const test = (v, loosely) => {
      // shape have to be the object, not an array or some simple type
      if (typeof v !== 'object' || Array.isArray(v)) {
        return false;
      }

      // value has not have additional props
      // (all of the v props have to be in s shape)
      const shape = !!loosely || Object.keys(v).every((e) => !!s[e]);

      // all of the value props have to be of the right type
      // (all of the s props have to be valid in v)
      const types = Object.keys(s).every((e) => s[e].test(v[e]));

      return types && shape;
    };

    return new LooseType({
      exactTest: (v) => test(v, false),
      looseTest: (v) => test(v, true),
    });
  },
  /* requiredFunc */
  /* requiredAny */
  /* customProp */
  /* customArrayProp */
  get any() {
    return new Type({
      test: (v) => typeof v !== 'undefined',
    });
  },
};

module.exports = Types;
