/* eslint-disable no-unused-expressions, no-unused-vars, import/no-extraneous-dependencies */
const { expect } = require('chai');
const Types = require('./Types');

// it('test of test', () => {
//   expect(true).to.be.true
// })

describe('array', () => {
  describe('not isRequired', () => {
    it('right type', () => {
      expect(Types.array.test([])).to.be.true;
    });
    it('undefined type', () => {
      expect(Types.array.test()).to.be.true;
    });
    it('wrong type', () => {
      expect(Types.array.test('')).to.be.false;
    });
  });
  describe('isRequired', () => {
    it('right type', () => {
      expect(Types.array.isRequired.test([])).to.be.true;
    });
    it('undefined type', () => {
      expect(Types.array.isRequired.test()).to.be.false;
    });
    it('wrong type', () => {
      expect(Types.array.isRequired.test('')).to.be.false;
    });
  });
});

describe('bool', () => {
  describe('not isRequired', () => {
    it('right type', () => {
      expect(Types.bool.test(true)).to.be.true;
    });
    it('undefined type', () => {
      expect(Types.bool.test()).to.be.true;
    });
    it('wrong type', () => {
      expect(Types.bool.test('')).to.be.false;
    });
  });
  describe('isRequired', () => {
    it('right type', () => {
      expect(Types.bool.isRequired.test(false)).to.be.true;
    });
    it('undefined type', () => {
      expect(Types.bool.isRequired.test()).to.be.false;
    });
    it('wrong type', () => {
      expect(Types.bool.isRequired.test('')).to.be.false;
    });
  });
});

describe('number', () => {
  describe('not isRequired', () => {
    it('right type', () => {
      expect(Types.number.test(1)).to.be.true;
    });
    it('undefined type', () => {
      expect(Types.number.test()).to.be.true;
    });
    it('wrong type', () => {
      expect(Types.number.test('')).to.be.false;
    });
  });
  describe('isRequired', () => {
    it('right type', () => {
      expect(Types.number.isRequired.test(1)).to.be.true;
    });
    it('undefined type', () => {
      expect(Types.number.isRequired.test()).to.be.false;
    });
    it('wrong type', () => {
      expect(Types.number.isRequired.test('')).to.be.false;
    });
  });
});

describe('string', () => {
  describe('not isRequired', () => {
    it('right type', () => {
      expect(Types.string.test('1')).to.be.true;
    });
    it('undefined type', () => {
      expect(Types.string.test()).to.be.true;
    });
    it('wrong type', () => {
      expect(Types.string.test(1)).to.be.false;
    });
  });
  describe('isRequired', () => {
    it('right type', () => {
      expect(Types.string.isRequired.test('1')).to.be.true;
    });
    it('undefined type', () => {
      expect(Types.string.isRequired.test()).to.be.false;
    });
    it('wrong type', () => {
      expect(Types.string.isRequired.test(1)).to.be.false;
    });
  });
});

describe('object', () => {
  describe('not isRequired', () => {
    it('right type', () => {
      expect(Types.object.test({})).to.be.true;
    });
    it('undefined type', () => {
      expect(Types.object.test()).to.be.true;
    });
    it('wrong type', () => {
      expect(Types.object.test([])).to.be.false;
    });
  });
  describe('isRequired', () => {
    it('right type', () => {
      expect(Types.object.isRequired.test({})).to.be.true;
    });
    it('undefined type', () => {
      expect(Types.object.isRequired.test()).to.be.false;
    });
    it('wrong type', () => {
      expect(Types.object.isRequired.test([])).to.be.false;
    });
  });
});

describe('any', () => {
  describe('not isRequired', () => {
    it('right type', () => {
      expect(Types.any.test({})).to.be.true;
      expect(Types.any.test('')).to.be.true;
      expect(Types.any.test(1)).to.be.true;
      expect(Types.any.test(true)).to.be.true;
    });
    it('undefined type', () => {
      expect(Types.any.test()).to.be.true;
    });
  });
  describe('isRequired', () => {
    it('right type', () => {
      expect(Types.any.isRequired.test({})).to.be.true;
      expect(Types.any.isRequired.test('')).to.be.true;
      expect(Types.any.isRequired.test(1)).to.be.true;
      expect(Types.any.isRequired.test(true)).to.be.true;
    });
    it('undefined type', () => {
      expect(Types.any.isRequired.test()).to.be.false;
    });
  });
});

describe('instanceOf', () => {
  class TestClass {}
  const inst = new TestClass();
  const t = Types.instanceOf(TestClass);
  describe('not isRequired', () => {
    it('right type', () => {
      expect(t.test(inst)).to.be.true;
    });
    it('undefined type', () => {
      expect(t.test()).to.be.true;
    });
    it('wrong type', () => {
      expect(t.test([])).to.be.false;
    });
  });
  describe('isRequired', () => {
    it('right type', () => {
      expect(t.isRequired.test(inst)).to.be.true;
    });
    it('undefined type', () => {
      expect(t.isRequired.test()).to.be.false;
    });
    it('wrong type', () => {
      expect(t.isRequired.test([])).to.be.false;
    });
  });
});

describe('oneOf', () => {
  const t = Types.oneOf(['Sunday', 'Monday']);
  describe('not isRequired', () => {
    it('right type', () => {
      expect(t.test('Sunday')).to.be.true;
      expect(t.test('Monday')).to.be.true;
    });
    it('undefined type', () => {
      expect(t.test()).to.be.true;
    });
    it('wrong type', () => {
      expect(t.test([])).to.be.false;
      expect(t.test(false)).to.be.false;
      expect(t.test('Friday')).to.be.false;
    });
  });
  describe('isRequired', () => {
    it('right type', () => {
      expect(t.isRequired.test('Sunday')).to.be.true;
      expect(t.isRequired.test('Monday')).to.be.true;
    });
    it('undefined type', () => {
      expect(t.isRequired.test()).to.be.false;
    });
    it('wrong type', () => {
      expect(t.isRequired.test([])).to.be.false;
      expect(t.isRequired.test(false)).to.be.false;
      expect(t.test('Friday')).to.be.false;
    });
  });
});

describe('oneOfType', () => {
  const t = Types.oneOfType([Types.string, Types.number]);
  describe('not isRequired', () => {
    it('right type', () => {
      expect(t.test('1')).to.be.true;
      expect(t.test(1)).to.be.true;
    });
    it('undefined type', () => {
      expect(t.test()).to.be.true;
    });
    it('wrong type', () => {
      expect(t.test([])).to.be.false;
      expect(t.test(false)).to.be.false;
    });
  });
  describe('isRequired', () => {
    it('right type', () => {
      expect(t.isRequired.test('1')).to.be.true;
      expect(t.isRequired.test(1)).to.be.true;
    });
    it('undefined type', () => {
      expect(t.isRequired.test()).to.be.false;
    });
    it('wrong type', () => {
      expect(t.isRequired.test([])).to.be.false;
      expect(t.isRequired.test(false)).to.be.false;
    });
  });
});

describe('shape', () => {
  const t = Types.shape({
    str: Types.string,
    strRec: Types.string.isRequired,
  });

  const rs1 = {
    str: 'string',
    strRec: 'string req',
  };
  const rs2 = {
    strRec: 'string req',
  };
  const ws1 = {
    str: 'string',
    strRec: 'string req',
    badProp: 'bad',
  };
  const ws2 = {
    str: 'string',
  };
  const wt1 = {
    str: 123,
    strRec: 'string req',
  };
  const wt2 = {
    strRec: 123,
  };
  describe('not isRequired', () => {
    it('right shape', () => {
      expect(t.test(rs1)).to.be.true;
      expect(t.test(rs2)).to.be.true;
    });
    it('undefined type', () => {
      expect(t.test()).to.be.true;
    });
    it('wrong shape', () => {
      expect(t.test(ws1)).to.be.false;
      expect(t.test(ws2)).to.be.false;
    });
    it('wrong type', () => {
      expect(t.test(wt1)).to.be.false;
      expect(t.test(wt2)).to.be.false;
    });
  });
  describe('isRequired', () => {
    it('right shape', () => {
      expect(t.isRequired.test(rs1)).to.be.true;
      expect(t.isRequired.test(rs2)).to.be.true;
    });
    it('undefined type', () => {
      expect(t.isRequired.test()).to.be.false;
    });
    it('wrong shape', () => {
      expect(t.isRequired.test(ws1)).to.be.false;
      expect(t.isRequired.test(ws2)).to.be.false;
    });
    it('wrong type', () => {
      expect(t.isRequired.test(wt1)).to.be.false;
      expect(t.isRequired.test(wt2)).to.be.false;
    });
  });

  describe('loose', () => {
    const x1 = {
      str: 'string',
      strRec: '1',
    };

    const x2 = {
      str: 'string',
      strRec: '1',
      plus: 1,
    };

    const x3 = {
      str: 'string',
      strRec: 1,
      plus: 1,
    };

    const x4 = {
      str: 'string',
      plus: 1,
    };

    it('exact test', () => {
      expect(t.test(x1)).to.be.true;
      expect(t.isRequired.test(x1)).to.be.true;
      expect(t.loose.exactTest(x1)).to.be.true;
      expect(t.loose.isRequired.exactTest(x1)).to.be.true;

      expect(t.test(x2)).to.be.false;
      expect(t.isRequired.test(x2)).to.be.false;
      expect(t.loose.exactTest(x2)).to.be.false;
      expect(t.loose.isRequired.exactTest(x2)).to.be.false;
    });

    it('loose test', () => {
      expect(t.looseTest(x2)).to.be.true;
      expect(t.isRequired.looseTest(x2)).to.be.true;
      expect(t.loose.test(x2)).to.be.true;
      expect(t.loose.isRequired.test(x2)).to.be.true;
    });

    it('undefined case', () => {
      expect(t.test()).to.be.true;
      expect(t.isRequired.test()).to.be.false;
      expect(t.loose.test()).to.be.true;
      expect(t.loose.isRequired.test()).to.be.false;

      expect(t.looseTest()).to.be.true;
      expect(t.isRequired.looseTest()).to.be.false;
      expect(t.loose.exactTest()).to.be.true;
      expect(t.loose.isRequired.exactTest()).to.be.false;
    });
  });
});

describe('arrayOf', () => {
  const t = Types.arrayOf(Types.oneOf(['Sunday', 'Monday']));
  describe('not isRequired', () => {
    it('right type', () => {
      expect(t.test(['Sunday', 'Monday'])).to.be.true;
      expect(t.test(['Sunday'])).to.be.true;
    });
    it('undefined type', () => {
      expect(t.test()).to.be.true; // empty value
      expect(t.test([])).to.be.true; // empty array
    });
    it('wrong type', () => {
      expect(t.test('')).to.be.false;
      expect(t.test(false)).to.be.false;
    });
  });
  describe('isRequired', () => {
    const t1 = Types.arrayOf(Types.oneOf(['Sunday', 'Monday']).isRequired);
    it('right type', () => {
      expect(t1.test(['Sunday', 'Monday'])).to.be.true;
      expect(t1.test(['Sunday'])).to.be.true;
    });
    it('undefined type', () => {
      expect(t1.isRequired.test()).to.be.false;
      expect(t.isRequired.test([])).to.be.true; // empty array
      expect(t1.isRequired.test([])).to.be.false; // empty array
    });
    it('wrong type', () => {
      expect(t.isRequired.test('')).to.be.false;
      expect(t.isRequired.test(false)).to.be.false;
    });
  });
});


// console.log('TEST',
//   Types.arrayOf(Types.oneOf(['string', 'number']))
//   .test(),
//   Types.oneOfType([Types.string, Types.number]).isRequired
//   .test()
// )

// eslint-disable-next-line no-console
console.log('ver', '20190906-19.11');
