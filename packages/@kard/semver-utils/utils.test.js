// eslint-disable-next-line import/no-extraneous-dependencies
const { expect } = require('chai');
const utils = require('./utils');

// dev ground btl
// -------------------------------------

// const { log } = console;
// log(utils.isValid('0.0.0-dev'));
// log(utils.isValid('1.0.0-rc.1+build.1'));
// log(utils.parse('1.0.0-rc.1+build.1'));
// log(utils.parse('1.0.0-rc.1'));
// log(utils.parse('1.0.0'));
// log(utils.parse('1.0')); // undefined
// log(utils.parse('1')); // undefined

// const ver = utils.parse('1.0.0-rc.1+build.1');
// log(utils.verToString(ver));

// const example = {
//   a: () => 1,
//   b() { return this.a() + 1; },
// };
// log(example.b());


describe('semver string validation', () => {
  it('positiv cases', () => {
    expect(utils.isValid('0.0.0-dev')).to.be.true;
    expect(utils.isValid('1.0.0-rc.1+build.1')).to.be.true;
    expect(utils.isValid('1.0.0-rc.1')).to.be.true;
    expect(utils.isValid('1.0.0')).to.be.true;
  });

  it('posnegativeitiv cases', () => {
    expect(utils.isValid('1.0')).to.be.false;
    expect(utils.isValid('1')).to.be.false;
  });
});
