// refs:
// * https://semver.org/
// * https://regex101.com/r/vkijKf/1/


/*
  type SemVer :: object {
    major, minor, patch, preRelease?, build?,
  }
 */

// eslint-disable-next-line no-unused-vars
const semverRegEx = /^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$/gm;

const utils = {

  coreVersionString: (vo) => `${vo.major}.${vo.minor}.${vo.patch}`,

  isValid: (str) => !!str.match(semverRegEx),

  parse(str) {
    if (!this.isValid(str)) {
      return undefined;
    }

    const vo = {};

    const majorStop = str.indexOf('.');
    vo.major = str.substring(0, majorStop);

    const minorStop = str.indexOf('.', majorStop + 1);
    vo.minor = str.substring(majorStop + 1, minorStop);

    const patchStop = str.indexOf('-', minorStop + 1);
    vo.patch = str.substring(
      minorStop + 1,
      patchStop > -1 ? patchStop : undefined,
    );
    if (patchStop === -1) { return vo; }

    const preReleaseStop = str.indexOf('+', patchStop + 1);
    vo.preRelease = str.substring(
      patchStop + 1,
      preReleaseStop > -1 ? preReleaseStop : undefined,
    );
    if (preReleaseStop === -1) { return vo; }

    // const preReleaseStop = str.indexOf('+', patchStop+1);
    vo.build = str.substring(preReleaseStop + 1);
    return vo;
  },

  verToString(vo) {
    const valid = typeof vo === 'object'
      && !Array.isArray(vo)
      && vo.major
      && vo.minor
      && vo.patch;
    if (!valid) { return undefined; }
    const preRelease = vo.preRelease ? `-${vo.preRelease}` : '';
    const build = vo.build ? `+${vo.build}` : '';
    return `${this.coreVersionString(vo)}${preRelease}${build}`;
  },
};

module.exports = utils;
