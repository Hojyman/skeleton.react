import { indexOfRegex } from './indexOfRegex';

/**
 * getProp utility - return value of props by its path or default value
 * @param {Object} object
 * @param {String} path
 * @param {*} defaultVal
 */
export default function getProp(obj, path) {
  // if object is not an object or the path is empty - it's the value, return it
  if (typeof obj !== 'object' || path === '') { return obj; }

  // if the first key is array selector open character or the dot after an array closed - drop it
  if (path.charAt(0) === '[' || path.charAt(0) === '.') {
    // eslint-disable-next-line no-param-reassign
    path = path.substr(1);
  }

  // get delimiter index
  const dli = indexOfRegex(path, /[.[\]]/, 0);

  // if the the path has no delimiters anymore - it's the final kay, return its value
  if (dli === -1) { return obj[path]; }

  // get key
  const key = path.substr(0, dli);

  // get next path (without processed key)
  const nextPath = path.substr(dli + 1);

  // get value of the key extracted
  const val = obj[key];

  // console.log(key, nextPath, val);
  return getProp(val, nextPath);
}
