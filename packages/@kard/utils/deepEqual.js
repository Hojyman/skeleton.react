const isPrimitive = (obj) => obj !== Object(obj);

const countProps = (obj, dropUndefinedProps) => {
  const keyList = Object.keys(obj);
  return dropUndefinedProps
    ? keyList
      .filter((e) => dropUndefinedProps && obj[e]).length
    : keyList.length;
};

export default function deepEqual(obj1, obj2, { dropUndefinedProps } = {}) {
  // it just the same object. No need to compare.
  if (obj1 === obj2) { return true; }

  if (!obj1 || !obj2) { return false; }

  // compare primitives
  if (
    isPrimitive(obj1)
    && isPrimitive(obj2)
  ) { return obj1 === obj2; }

  if (
    countProps(obj1, dropUndefinedProps)
    !== countProps(obj2, dropUndefinedProps)
  ) { return false; }

  // compare objects with same number of keys
  /* eslint-disable guard-for-in, no-restricted-syntax */
  for (const key in obj1) {
    if (
      dropUndefinedProps
      && !obj1[key]
      && !(key in obj2)
    ) { return true; }

    // onj2 object doesn't have this prop
    // if (!(key in obj2)) { return false; }

    // call itself recursively
    if (
      !deepEqual(obj1[key], obj2[key], { dropUndefinedProps })
    ) { return false; }
  }

  return true;
}
