export default function Subscription() {
  const subscribers = [];
  this.subscribe = (func) => {
    subscribers.push(func);
    return function unsubscribe() {
      const i = subscribers.indexOf(func);
      if (i === -1) { return; }
      // eslint-disable-next-line consistent-return
      return subscribers.splice(i, 1)[0]; // subscriber
    };
  };
  this.broadcast = (data) => subscribers.forEach((f) => f(data));
  this.scan = (data) => [data, ...subscribers].reduce((acc, f) => f(acc));
}
