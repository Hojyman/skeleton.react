/* eslint-disable import/prefer-default-export */

/**
 * indexOfRegex utility - an alternative to lodash.get
 * @param {Object} text - text to look in
 * @param {Regex}  re   - regex to look for
 * @param {Number} i    - index to look from
 */
export const indexOfRegex = (text, re, i = 0) => {
  const indexInSuffix = text.slice(i).search(re);
  return indexInSuffix < 0 ? indexInSuffix : indexInSuffix + i;
};
