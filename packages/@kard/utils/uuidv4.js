/**
 * GUID / UUID generator
 * ref: https://stackoverflow.com/a/2117523
 * @return {[type]} [description]
 */
export default function () {
  return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11)
    /* eslint-disable-next-line no-bitwise, no-mixed-operators */
    .replace(/[018]/g, (c) => (c ^ window.crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4)
      .toString(16));
}
