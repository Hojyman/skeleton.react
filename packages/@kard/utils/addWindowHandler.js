/**
 * [description]
 * @param  {[type]} hid     [description]
 * @param  {[type]} handler [description]
 * @return {[type]}         [description]
 * @example
 *   addWindowHandler('onblur', () => chrome.contextMenus.removeAll());
 */
export default (hid, handler) => {
  const originHandler = window[hid];
  window[hid] = (...args) => {
    const resultCandidate = originHandler
      ? originHandler(...args)
      : null;
    return handler(...args, resultCandidate);
  };
};
