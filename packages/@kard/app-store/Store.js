import getProp from '@kard/utils/getProp';
import Subscription from '@kard/utils/Subscription';
import cloneObject from '@kard/utils/cloneObject';

export default function Store(initial) {
  const state = cloneObject(initial);
  const subscription = new Subscription();
  const using = new Subscription();

  // experimental
  const getStateOf = (selector = {}) => {
    const result = {};
    Object.keys(selector).forEach((key) => {
      const val = getProp(state, selector[key]);
      // make sure we don't supply reference to a part of the original state
      result[key] = cloneObject(val);
    });
    return result;
  };

  this.subscribe = (f) => subscription.subscribe(f);
  this.use = (f) => using.subscribe(f(this.getState));

  // this.subscribe = (func) => {
  //   func(this); // initial firing
  //   return subscription.subscribe(func);
  // };

  this.setState = (update) => {
    // make sure we don't take a reference to the app's internal structures
    let acc = cloneObject(update);
    // apply middlewares
    acc = using.scan(cloneObject(acc));
    Object.keys(acc).forEach((p) => {
      state[p] = acc[p];
    });
    // notify subscribers about the state change
    subscription.broadcast(this);
  };

  this.getState = (selector) => {
    if (!selector) {
      return cloneObject(state);
    }
    return getStateOf(selector);
  };
}
