import { expect } from 'chai';
import { deepEqual } from '../utils';

import Store from './Store';

const isStoreShape = ({
  subscribe,
  use,
  setState,
  getState,
}) => (
  typeof subscribe === 'function'
  && typeof use === 'function'
  && typeof setState === 'function'
  && typeof getState === 'function'
);

describe('@kard/app-store', () => {
  const initialState = { valA: 1, set: { valB: 1 } };
  const store = new Store(initialState);
  let unsubscribe = null;
  function subscriber() {}

  describe('basic state operations', () => {
    it('initialState has to be set', () => {
      expect(deepEqual(store.getState(), initialState)).to.equal(true);
    });

    it('getState has return mapped state by selectors', () => {
      const selectors = { varA: 'valA', varB: 'set.valB', varD: 'set' };
      const awaited = { varA: 1, varB: 1, varD: { valB: 1 } };
      expect(deepEqual(store.getState(selectors), awaited)).to.equal(true);
    });

    it('setState has to set the whole state and its parts', () => {
      const newState = { valA: 2, set: { valB: 3 }, valC: 4 };
      store.setState({ valA: 2 });
      store.setState({ set: { valB: 3 } });
      store.setState({ valC: 4 });
      expect(deepEqual(store.getState(), newState)).to.equal(true);
      store.setState(initialState);
      expect(deepEqual(store.getState(), { ...initialState, valC: 4 }))
        .to.equal(true);
    });
  });


  describe('subscription management', () => {
    it('subsctibe should return unsubscribe function', () => {
      unsubscribe = store.subscribe(subscriber);
      expect(typeof unsubscribe === 'function').to.equal(true);
    });

    it('unsubscribe should return subscriber reference', () => {
      expect(unsubscribe()).to.equal(subscriber);
    });

    it('repeated unsubscribe call should return undefined', () => {
      expect(unsubscribe()).to.equal(undefined);
    });

    it('subsctiber subscriber should get updated state',
      (done) => {
        let unsub = null;
        const update = { valA: 2 };
        const updatetState = { ...store.getState(), ...update };
        const subscribr = (stor) => {
          expect(isStoreShape(stor)).to.equal(true);
          expect(deepEqual(stor.getState(), updatetState)).to.equal(true);
          unsub();
          done();
        };

        unsub = store.subscribe(subscribr);
        store.setState(update);
      });
  });

  describe('middleware management', () => {
    it('middleware should get right state and update values, be able to change the update',
      (done) => {
        const update = { newVal: 123 };
        const state = store.getState();
        const nextState = {
          valA: 2, set: { valB: 1 }, valC: 4, newVal: 123, anotherVal: 456,
        };
        const middleware = (getState) => (updt) => {
          expect(deepEqual(getState(), state)).to.equal(true);
          expect(deepEqual(updt, update)).to.equal(true);
          return { ...update, anotherVal: 456 };
        };
        const unsubscribeMiddleware = store.use(middleware);
        const unsubscrb = store.subscribe((stor) => {
          expect(deepEqual(stor.getState(), nextState)).to.equal(true);
          unsubscrb();
          unsubscribeMiddleware();
          done();
        });
        store.setState({ newVal: 123 });
      });
  });
});
