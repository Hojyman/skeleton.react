/**
 * @example
 *   const coded = encodeString(str, passphrase);
 *   const decoded = decodeString(coded, passphrase);
 *
 *   console.log('coded', coded);
 *   console.log('decoded', decoded);
 */

const encodeChar = (char, keyChar) => char.charCodeAt(0) + keyChar.charCodeAt(0); // => number
const decodeChar = (num, keyChar) => String.fromCharCode(num - keyChar.charCodeAt(0)); // => char

const getKeyCharFor = (keyStr, idx) => keyStr[idx % keyStr.length];

const encodeString = (str, passphrase) => str.split('').map((c, i) => encodeChar(c, getKeyCharFor(passphrase, i)));
const decodeString = (arr, passphrase) => arr.map((c, i) => decodeChar(c, getKeyCharFor(passphrase, i))).join('');


/**
 * @example
 *   const encoder = new Encoder('secret password');
 *   const coded = encoder.encodeString('string to be encoded')
 *   const decoded = encoder.decodeString(coded)
 *
 *   console.log('coded', coded);
 *   console.log('decoded', decoded);
 */
export default function Encoder(passphrase) {
  this.encodeString = (str) => encodeString(str, passphrase);
  this.decodeString = (arr) => decodeString(arr, passphrase);
}

/**
 * @example
 *   const encoder = new Encoder('secret password');
 *   const coded = encoder.encodeString('string to be encoded').join('.')
 *   const decoded = encoder.decodeString(coded.split('.'))
 *
 *   console.log('coded', coded);
 *   console.log('decoded', decoded);
 */

// const encoder = new Encoder('secret password');
// const coded = encoder.encodeString('string to be encoded').join('.');
// const decoded = encoder.decodeString(coded.split('.'));
// console.log('coded >>>', coded);
// console.log('decoded >>>', decoded);
