/* eslint-disable no-underscore-dangle, react/jsx-props-no-spreading */

import React from 'react';

const _AppContext = React.createContext('light');

export function connect(mapStateToProps) {
  /* eslint-disable react/static-property-placement, camelcase, react/sort-comp */
  return (WrappedComponent) => class extends React.Component {
    static contextType = _AppContext;

    UNSAFE_componentWillMount() {
      const { $model } = this.context;
      this.unsubscribe = $model.subscribe(this.setAppContext);
      this.setAppContext();
    }

    componentWillUnmount() {
      this.unsubscribe();
    }

    setAppContext = () => {
      const { config, $model, controller } = this.context;
      this.setState({ mapState: $model.getState, config, controller });
    }

    render() {
      const props = mapStateToProps(this.state, this.props);
      return <WrappedComponent {...props} />;
    }
  };
}

export default _AppContext.Provider;
