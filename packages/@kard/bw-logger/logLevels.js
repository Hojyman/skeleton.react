const logLevels = {
  FATAL: 100,
  ERROR: 200,
  WARN: 300,
  INFO: 400,
  DEBUG: 500,
};

export default logLevels;
