import color from './color';
import logLevels from './logLevels';
import { getTimeStamp } from './logUtils';

/* eslint-disable no-underscore-dangle, max-len */

const createLogger = ({
  logId = '', // logIdDefault, // default log id to use
  logIdColor = color.FgWhite, // defaukt log id color to use
  logLevel = logLevels.INFO, // default logLevel to set at the start
  labelFormat = '[HH:MM:ss.SSS LID5]', // LID5 - the only supported for now
} = {}) => {
  /**
   * logLevel - private variable keeps the currently set logLevel
   * @type {Number}
   */
  let _logLevel = logLevel; // default is 400

  /**
   * logId - private variable contains the currently used log id (clause before data/type breckets)
   * @type {String}
   */
  let _logId = logId;

  /**
   * logMessage - private function which actually performs the real console output.
   * Called in context of the created logger
   * @param  {String}   id   - id og the logger to use
   * @param  {...Any}   args - arguments to put to console
   * @return {Void}
   */
  function logMessage(id, ...args) {
    const { config } = this[id];
    // const logIdString = `${logIdColor}${this.logId}${color.reset}`;
    const logIdString = `${logIdColor}${this.logId}${color.reset}`;
    if (config.level <= this.logLevel) {
      const logid = `${config.color}${id.toUpperCase()}${logIdColor}`;
      const pre = `${logIdString} ${
        getTimeStamp(
          labelFormat
            .replace('LID5', `${logid}${' '.repeat(5 - id.length)}`),
        )
      }`;
      // eslint-disable-next-line no-console
      console.log(`${pre}`, ...args);
    }
  }

  /**
   * logger - the base of the creating logger
   * @type {Object}
   */
  const logger = {

    /**
     * logId - getter which return the curently used logger id
     * @return {String} - the curently used logger id
     */
    get logId() { return _logId; },
    set logId(val) { _logId = val; },

    /**
     * logLevel - getter returns the currently set logLevel (max level of log output)
     * Default is 400
     * @return {Number} [description]
     */
    get logLevel() { return _logLevel; },

    /**
     * setLogLevel - logLevel setter
     * @param {Number} val - new logLevel to set
     */
    set logLevel(val) { _logLevel = val; },

    /**
     * getColor - getter returns the color string by it's id
     * @param  {String} id - color id (Bright, FgRed, BgGreen, etc.)
     * @return {String} color prefix value
     */
    getColor(id) { return color[id]; },

    /**
     * addLogLevel - public method which allows to add custom levels to the logger
     * @param {String} id    - id of the custom level
     * @param {Numbet} level - level value
     * @param {String} clr   - colos string acceptable inthe console output
     */
    addLogLevel(id, level, clr = color.FgBlue) {
      this[id] = (...x) => logMessage.call(this, id, ...x);
      this[id].config = { level, color: clr };
    },
  };

  /**
   * Standard logger methods initialization
   */
  logger.addLogLevel('debug', logLevels.DEBUG, color.FgMagenta);
  logger.addLogLevel('info', logLevels.INFO, color.FgGreen);
  logger.addLogLevel('warn', logLevels.WARN, color.FgYellow);
  logger.addLogLevel('error', logLevels.ERROR, color.FgRed);
  logger.addLogLevel('fatal', logLevels.FATAL, color.FgRed + color.Bright);

  return logger;
};


/* examples */

// const _logger = createLogger();
// console.log(_logger);

// _logger.fatal('fatal');
// _logger.error('error');
// _logger.warn('warn');
// _logger.info('info');
// _logger.debug('debug');

// _logger.addLogLevel('help', 450, color.FgBlue);
// _logger.help('help NO OUTPUT');

// _logger.logLevel = 500;
// _logger.help('help');
// _logger.debug('Fuck!');

/* eslint-disable no-underscore-dangle */
// module.exports = createLogger;

export default createLogger;
