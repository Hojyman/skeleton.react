import Logger from './Logger';
import colors from './color';
import logLevels from './logLevels';

export { Logger, colors, logLevels };
export default Logger;
